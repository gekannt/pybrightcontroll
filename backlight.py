#!/usr/bin/python
# 31.05.15
__author__ = 'Alex Shulzhenko me@alexshulzhenko.ru'


import optparse

# Default buttons didn't work for me. As a workaround you can do
# direct echo to the files listed below to control it. I thoght of optimisation of this task this is how this script
#  was born
# system files used for controlling brightness
file = '/sys/class/backlight/intel_backlight/max_brightness'
destination_file = '/sys/class/backlight/intel_backlight/brightness'


def increase(step, value, max_value):
    valueToSet = int(value) + int(step)

    if int(valueToSet) > int(max_value):
        valueToSet = max_value

    try:
        f=open(destination_file,'w+')
    except IOError:
        print 'cant open file '

    f.write(str(valueToSet))
    f.close()
    return valueToSet


def decrease(step,value):

    valueToSet = int(value) - int(step)

    if valueToSet <= 0:
        valueToSet = 10 # at least some small value

    try:
        f=open(destination_file,'w+')
    except IOError:
        print 'cant open file '

    f.write(str(valueToSet))
    f.close()
    return  valueToSet


def main():

    p = optparse.OptionParser()
    p.add_option('--decrease','-d')
    p.add_option('--increase','-i')

    options, arguments = p.parse_args()

    if  options.decrease == None and options.increase == None:
        print 'No arguments specified, you should enter  either name.py -i number or name.py -d number, ' \
              'e.g.  name.py -i 100 will increase brightness by 100 points'
        return True



    f=open(file,'r')

    max_value = 0
    for line in f:
        max_value = line
        break

    f.close()

    curr_value = 0
    f = open(destination_file,'r')
    for line in f:
        curr_value = line
        break

    f.close()

    print 'current value is ', curr_value

    print 'max_value  is',max_value

    if options.decrease != None:
        print 'new value is ',decrease(options.decrease,curr_value)
        return True

    if options.increase != None:
        print 'new value is ',increase(options.increase, curr_value, max_value)
        return True




if __name__ == '__main__':
  main()